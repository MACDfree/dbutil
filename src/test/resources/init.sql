
create table `test` (
  `id` long not null auto_increment comment '主键',
  `text` varchar(50) not null comment '备注',
  primary key (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into `test` (`text`) values ( '123' );

insert into `test` (`text`) values ( '1233' );