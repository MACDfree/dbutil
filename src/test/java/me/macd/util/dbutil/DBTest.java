package me.macd.util.dbutil;

import org.junit.jupiter.api.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

/**
 * @author macd
 * @version 1.0 [2019-06-11 22:50]
 **/
@DisplayName("DB test")
class DBTest {
    static {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private DB db;

    @BeforeEach
    void beforeEach() throws SQLException, IOException {
        Connection connection = DriverManager.getConnection("jdbc:h2:mem:test", "root", "");
        db = new DB(connection);
        InputStream in = getClass().getResourceAsStream("/init.sql");
        Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);

        StringBuilder sb = new StringBuilder();
        char[] buff = new char[1024];
        int count;
        while ((count=reader.read(buff))>0) {
            sb.append(buff,0,count);
        }
        db.execute(sb.toString());

    }

    @AfterEach
    void afterEach() throws IOException {
        db.close();
    }

    @Test
    void update() throws SQLException {
        int r = db.update("insert into test (text) values ('123')");
        Assertions.assertEquals(1, r);
    }

    @Test
    void queryForObject() throws SQLException {
        Test1 t = db.queryForObject("select * from test where id=?", Test1.class, 1);
        Assertions.assertEquals(1, t.getId());
        Assertions.assertEquals("123", t.getText());
    }

    @Test
    void queryForList() throws SQLException {
        List<Test1> list = db.queryForList("select * from test", Test1.class);
        Assertions.assertEquals(2, list.size());
    }

    public static class Test1 {
        private Long id;
        private String text;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}